- more logging and error handling, find better way than assert!!
- more use of navcomponent
- rethink reset functions of viewmodels
- investigate blinking in tableheader
- backgrounds in table can be done more concisely using LayerDrawable
- use one player fragment for both players, allows for faster switching and saving of states, plus you can get rid of requestplayer interface
- playerprofiles
- explainers
- restructure all fields Xplayer1Y and Xplayer2Y into an array
- sort resources

# for release
- cleanup