package org.sbv.pockettracker.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;

import androidx.appcompat.widget.PopupMenu;

import android.widget.Toast;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentContainerView;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.preference.PreferenceManager;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigationrail.NavigationRailView;

import org.sbv.pockettracker.model.Players;
import org.sbv.pockettracker.model.PlayersViewModel;
import org.sbv.pockettracker.model.PoolTableViewModel;
import org.sbv.pockettracker.R;
import org.sbv.pockettracker.model.ScoreBoard;
import org.sbv.pockettracker.model.ScoreBoardViewModel;
import org.sbv.pockettracker.model.ScoreSheetWriter;
import org.sbv.pockettracker.utils.ScoreSheetIO;
import org.sbv.pockettracker.model.ScoreSheetViewModel;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements CounterFragment.CounterFragmentListener, ScoreSheetFragment.ScoreSheetFragmentListener, StatisticsFragment.StatisticsFragmentListener, NumberPaneFragment.NumberPaneFragmentProvider, PlayerFragment.PlayerFragmentProvider{

    private PlayersViewModel playersViewModel;
    private ScoreBoardViewModel scoreBoardViewModel;
    private PoolTableViewModel poolTableViewModel;
    private ScoreSheetViewModel scoreSheetViewModel;
    private SharedPreferences preferences;
    private NavController navController;
    private MaterialButton dropdownButton;
    private SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener;


    private ActivityResultLauncher<Intent> createFileActivityLauncher;
    private ActivityResultLauncher<Intent> readFileActivityLauncher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        applyPreferences();

        assignViewModels();
        
        applyNavigation();

        if (savedInstanceState == null){
            FragmentContainerView counterFragmentContainer = findViewById(R.id.counterFragmentContainer);
            if (counterFragmentContainer != null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.counterFragmentContainer, new CounterFragment())
                        .commit();
            }
        }

        createFileActivityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult o) {
                        if (o.getResultCode() == Activity.RESULT_OK) {
                            Intent data = o.getData();
                            if (data != null && data.getData() != null) {
                                Uri uri = data.getData();
                                try (OutputStream outputStream = getContentResolver().openOutputStream(uri);
                                     OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream)) {
                                    ScoreSheetIO.writeToFile(outputStreamWriter, Objects.requireNonNull(playersViewModel.getPlayers().getValue()), Objects.requireNonNull(scoreSheetViewModel.getScoreSheet().getValue()));
                                    Toast.makeText(MainActivity.this, "Game saved successfully!", Toast.LENGTH_SHORT).show();
                                } catch (IOException e) {
                                    Toast.makeText(MainActivity.this, "Failed to save game:" + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "Failed to save game: no data returned", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Log.d("IO", "MainActivity.createFileActivityLauncher in Callback: Failed to load game: operation cancelled");
                        }
                    }
                }
        );

        readFileActivityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult o) {
                        if (o.getResultCode() == Activity.RESULT_OK) {
                            Intent data = o.getData();
                            if (data != null && data.getData() != null) {
                                Uri uri = data.getData();
                                try (InputStream inputStream = getContentResolver().openInputStream(uri);
                                     InputStreamReader inputStreamReader = new InputStreamReader(inputStream)) {
                                    onNewGameButtonClick();
                                    ScoreSheetIO.readFromFile(inputStreamReader, playersViewModel, new ScoreSheetWriter(scoreSheetViewModel, poolTableViewModel, scoreBoardViewModel));
                                    Toast.makeText(MainActivity.this, "Game loaded successfully!", Toast.LENGTH_SHORT).show();
                                } catch (IOException e) {
                                    Toast.makeText(MainActivity.this, "Failed to load game:" + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }else{
                                Toast.makeText(MainActivity.this, "Failed to load game: no data returned" , Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Log.d("IO", "MainActivity.readFileActivityLauncher in Callback: Failed to load game: operation cancelled");
                        }
                    }
                });
    }
    
    private void assignViewModels(){
        playersViewModel = new ViewModelProvider(this).get(PlayersViewModel.class);

        poolTableViewModel = new ViewModelProvider(this).get(PoolTableViewModel.class);

        scoreBoardViewModel = new ViewModelProvider(this).get(ScoreBoardViewModel.class);

        scoreSheetViewModel = new ViewModelProvider(this).get(ScoreSheetViewModel.class);
    }
    
    private void applyPreferences(){
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        String themePreference = preferences.getString("theme","system");
        switch (themePreference){
            case "light":
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
            case "dark":
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;
            default:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                break;
        }

        String stringWinnerPoints = preferences.getString("winnerPoints_default", "40");
        try{
            ScoreBoard.defaultWinnerPoints = Integer.parseInt(stringWinnerPoints);
        } catch (NumberFormatException ne){
            Log.d("Bad preference", "In MainActivity.onCreate: winnerpoints is not saved as a parseable String!", ne);
            ScoreBoard.defaultWinnerPoints = 40;
        }

        Players.defaultPlayerNames[Players.PLAYER_1_NUMBER] = preferences.getString("player1_name_default", "");
        Players.defaultPlayerNames[Players.PLAYER_2_NUMBER] = preferences.getString("player2_name_default", "");
        Players.defaultPlayerClubs[Players.PLAYER_1_NUMBER] = preferences.getString("player1_club_default", "");
        Players.defaultPlayerClubs[Players.PLAYER_2_NUMBER] = preferences.getString("player2_club_default", "");
        Players.haveClubs = preferences.getBoolean("club_toggle", true);

        sharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, @Nullable String key) {
                if (key != null) {
                    if (key.equals("theme")){
                        recreate();
                    }
                }
            }
        };
        preferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }

    private void applyNavigation(){
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
        navController = navHostFragment.getNavController();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        if (bottomNavigationView != null) {
            NavigationUI.setupWithNavController(bottomNavigationView, navController);
        }

        NavigationRailView navigationRailView = findViewById(R.id.rail_navigation);
        if (navigationRailView != null) {
            NavigationUI.setupWithNavController(navigationRailView, navController);
        }

        dropdownButton = findViewById(R.id.dropdown_button);
        dropdownButton.setOnClickListener(this::showDropdownMenu);
    }

    private void showDropdownMenu(View anchor){
        PopupMenu dropdownMenu = new PopupMenu(this, anchor);
        MenuInflater menuInflater = dropdownMenu.getMenuInflater();
        dropdownMenu.setForceShowIcon(true);
        menuInflater.inflate(R.menu.dropdown_menu, dropdownMenu.getMenu());

        dropdownMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.settings_dropdown){
                //NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.counterFragment, true).build();
                Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
            } else if (item.getItemId() == R.id.saveGame_dropdown){
                onSaveButtonClick();
                return true;
            } else if (item.getItemId() == R.id.newGame_dropdown){
                onNewGameButtonClick();
                return true;
            } else if (item.getItemId() == R.id.loadGame_dropdown){
                onLoadButtonClick();
                return true;
            } else {
                return false;
            }
        });

        dropdownMenu.show();
    }

    private void assignPoints(){
        int points = poolTableViewModel.evaluate();
        scoreBoardViewModel.addPoints(scoreSheetViewModel.turnplayerNumber(), points);
    }

    public void onNewGameButtonClick(){
        if (scoreSheetViewModel.length() > 1) {
            AlertDialog.Builder confirmDialog = new AlertDialog.Builder(this);
            confirmDialog.setMessage(getString(R.string.confirmNewGame))
                    .setPositiveButton(getString(R.string.positive), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            resetGame();
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton(getString(R.string.negative), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            confirmDialog.show();
        }
    }

    private void resetGame(){
        playersViewModel.reset();

        poolTableViewModel.reset();

        scoreBoardViewModel.reset();

        scoreSheetViewModel.reset();
    }

    public void onSaveButtonClick() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");

        Date now = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        formatter.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
        String nameProposal = "game_" + formatter.format(now) + ".csv";
        intent.putExtra(Intent.EXTRA_TITLE, nameProposal);

        createFileActivityLauncher.launch(intent);
    }

    public void onLoadButtonClick() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        readFileActivityLauncher.launch(intent);
    }

    //numberpanelistener methods
    @Override
    public void onNumberPaneClick(int number){
        poolTableViewModel.updateNumberOfBalls(number);
        if (number == 1) {
            assignPoints();
        }
    }

    //playerfragmentprovider methods
    @Override
    public void onNameInput(int playerNumber, String name){
        playersViewModel.updatePlayerName(playerNumber, name);
    }
    @Override
    public void onClubInput(int playerNumber, String club){
        playersViewModel.updateClubName(playerNumber, club);
    }
    @Override
    public void onSwapButtonClick(){
        playersViewModel.swap();
    }


    @Override
    public void onStatisticsButtonClick(){
        NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.scoreSheetFragment, true).build();
        navController.navigate(R.id.action_scoreSheetFragment_to_statisticsFragment, null, navOptions);
    }

    @Override
    public void onScoreSheetButtonClick(){
        navController.navigate(R.id.action_statisticsFragment_to_scoreSheetFragment);
    }

    @Override
    public void onPlayerCardClick(int playerNumber) {
        PlayerFragment playerFragment = PlayerFragment.newInstance(playerNumber);
        playerFragment.show(getSupportFragmentManager(), "Player"+playerNumber+1+"Fragment");
    }

    @Override
    public void onBallsOnTableFloatingButtonClick() {
        NumberPaneFragment numberPaneFragment = NumberPaneFragment.newInstance(poolTableViewModel.getOldNumberOfBalls());
        numberPaneFragment.show(getSupportFragmentManager(), "NumberPane");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        preferences.unregisterOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }
}