# PocketTRacker ![](app/src/main/res/mipmap-xxhdpi/ic_launcher.webp)
=============================

**A little app to keep track of your straight pool games.**

A pool player myself, I was always annoyed by writing the score sheet for straight pool games.
So here is a digital tool to get rid of this paperwork! 
It automatically keeps tabs on the scores. All you have to do is enter the number of balls after every turn and why the turn ended.

Apart from just taking care of the math, the app also produces a full scoresheet and a game analysis with graphics!
If you want to continue your games later (or want the data as a memory), you can save and load them.

*I have a lot of [additional features I plan on implementing](Ideas.md). Help is always welcome of course.*

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.sbv.pockettracker/)

[<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png"
     alt="Get it on IzzyOnDroid"
     height="80">](https://apt.izzysoft.de/fdroid/index/apk/org.sbv.pockettracker/)

Or download the latest APK from the [Releases Section](https://gitlab.com/Dacid99/pockettracker/-/releases)