# Current Version
- allow changing of player names
- with player clubs
- keeps track of the scores
- undo and redo button
- player focus and winner indicator
- scores are changed by difference of balls on table
- numberpane for entering of remaining balls
- entering of reason for end of turn
- scoresheet viewer
- some statistics for players
- save and load games
- options menu for customization
- settings for toggling options like club on/off
- remember players via defaults and autocomplete
- AOD feature
- statistics for players and profiles with plots
- scoresheet with coloring of highest run etc
- tablet layouts

# Future features
- remember players via profiles
- editing of scores in scoresheet
- option of tracking by every pot
- snookertracker in same app
- maybe get number of remaining balls by camera (for users who are too lazy to count i guess)